const posts = [];
const count = 1;

document.querySelector('#form-add-post').addEventListener('submit', (e) => {
  e.preventDefault();

  postMessage.push({
    id: count,
    title: document.querySelector("txt-title"),
    body: document.querySelector("txt-body")
  });

  count++;

  showPosts(posts);
  alert("Successfully added.");
  
  
  const showPosts = (posts) => {
    let postEntries = '';
    
    posts.forEach((post)=>{
      postEntries += `
			<div id = "post-${post.id}">
      <h3 id="post-title-${post.id}">${post.title}</h3>
      <p id="post-body-${post.id}">${post.body}</p>
      <button onclick="editPost(${post.id})">Edit</button>
      <button onclick="deletePost(${post.id})">Delete</button>
			</div>
      `
    })
    document.querySelector('#div-post-entries').innerHTML = postEntries;
  }
})a